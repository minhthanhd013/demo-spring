package springbean;

public class Employee {
    private String position = "Employee";
    private String name;
    private int age;
    private JobCenter theJobCenter;
//

    public Employee() {
        System.out.println("You're in no args springBeanDemo()");
    }

    public void setTheJobCenter(JobCenter theJobCenter) {
        this.theJobCenter = theJobCenter;
    }
    //    public springBeanDemo(job myJob) {
//        this.myJob = myJob;
//    }

    public Employee(JobCenter theJobCenter) {
        System.out.println("You're in springBeanDemo()");
        this.name = "123";
        this.age = 1;
        this.theJobCenter = theJobCenter;
    }

    public Employee(String name, int age, JobCenter myJob) {
        System.out.println("You're in springBeanDemo() with 3 args");
        this.name = name;
        this.age = age;
        this.theJobCenter = myJob;
    }
    public Employee(String name, int age) {
        System.out.println("You're in springBeanDemo() with 2 args");
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public void sayHello(){
        System.out.println("My name is: "+this.name+"\nMy age: "+this.age+"\n"+this.theJobCenter.getJob()+"\nMy position is: "+this.position);
    }
    public void startUp(){
        System.out.println("Inside startUp() method");
    }
    public void shutDown(){
        System.out.println("Inside shutDown() method");
    }
}
