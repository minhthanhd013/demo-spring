package springbean;

public class Dentist implements JobCenter{
    @Override
    public String getJob() {
        return "Fill teeth for the customer";
    }
}
