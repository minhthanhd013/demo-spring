package springbean;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        // Demo Bean IOC & DI
        Employee beanDemo =  context.getBean("temp", Employee.class);

        beanDemo.sayHello();
        System.out.println();
        // Demo Bean Scope:
        Admin beanDemo1 =  context.getBean("temp1", Admin.class);
        Admin beanDemo2 =  context.getBean("temp1", Admin.class);
        //  Singleton:
        System.out.println("beanDemo1 in hashCode(): "+beanDemo1.hashCode());
        System.out.println("beanDemo2 in hashCode(): "+beanDemo2.hashCode());
        System.out.println("Is equal: "+(beanDemo1.equals(beanDemo2)));
        System.out.println();
        //  Prototype:
        Admin beanDemo3 =  context.getBean("temp2", Admin.class);
        Admin beanDemo4 =  context.getBean("temp2", Admin.class);
        System.out.println("beanDemo3 in hashCode(): "+beanDemo3.hashCode());
        System.out.println("beanDemo4 in hashCode(): "+beanDemo4.hashCode());
        System.out.println("Is equal: "+(beanDemo3.equals(beanDemo4)));
        System.out.println();
        context.close();
    }
}
