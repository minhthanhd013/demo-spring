package springannotation;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import springannotation.temp.Admin;
import springannotation.temp.Employee;

public class Main {
    public static void main(String[] args) {
    // Config with XML file:
        ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Admin admin = context.getBean( "Admin",Admin.class);
        System.out.println(admin.getJob());
        System.out.println(admin.getPhone());
        Employee employee = context.getBean("Employee", Employee.class);
        System.out.println(employee.getJob());

        // Demo SCOPE and LIFE CYCLES:
        // SCOPE SINGLETON:
        Admin admin1 =  context.getBean( "Admin",Admin.class);
        Admin admin2 =  context.getBean( "Admin",Admin.class);
        System.out.println("Admin 1 hashCode(): "+ admin1.hashCode());
        System.out.println("Admin 2 hashCode(): "+ admin2.hashCode());
        System.out.println("Check if is equal: "+(admin1.equals(admin2)));
        // SCOPE PROTOTYPE
        Employee employee1 =  context.getBean( "Employee",Employee.class);
        Employee employee2 =  context.getBean( "Employee",Employee.class);
        System.out.println("employee 1 hashCode(): "+ employee1.hashCode());
        System.out.println("employee 2 hashCode(): "+ employee2.hashCode());
        System.out.println("Check if is equal: "+(employee1.equals(employee2)));
        context.close();
    }
}
