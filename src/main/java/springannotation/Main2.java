package springannotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springannotation.temp.Admin;
import springannotation.temp.Config;

public class Main2 {
    public static void main(String[] args) {
        // Config without XML file:
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        Admin admin = context.getBean("Admin", Admin.class);
        System.out.println(admin.getJob());
        context.close();
    }
}
