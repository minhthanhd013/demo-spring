package springannotation.temp;

import org.springframework.stereotype.Component;

@Component
public class Iphone implements Phone{
    @Override
    public String getTask() {
        return "Iphone";
    }
}
