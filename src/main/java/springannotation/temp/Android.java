package springannotation.temp;

import org.springframework.stereotype.Component;

@Component
public class Android implements Phone{
    @Override
    public String getTask() {
        return "Android phone";
    }
}
