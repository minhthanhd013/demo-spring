package springannotation.temp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component("Admin")
public class Admin implements Company{
    @Autowired
    @Qualifier("iphone")
    private Phone phone ;

//    @Autowired
//    public Admin(Phone phone) {
//        this.phone = phone;
//    }


    public Admin() {
    }

//    public void setPhone(Phone phone) {
//        this.phone = phone;
//    }

    /**
     * Life cycle:
     */
    @PostConstruct
    public void startUp(){
        System.out.println("Inside startUp()");
    }
    @PreDestroy
    public void shutDown(){
        System.out.println("Inside shutDown()");
    }
    @Override
    public String getJob() {
        return "I'm an Admin my job is review the document this weekend";
    }

    @Override
    public String getPhone() {
        return phone.getTask();
    }
}
