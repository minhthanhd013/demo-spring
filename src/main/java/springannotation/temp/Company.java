package springannotation.temp;

public interface Company {
    String getJob();
    String getPhone();
}
