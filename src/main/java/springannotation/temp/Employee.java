package springannotation.temp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("Employee")
@Scope("prototype")
public class Employee implements Company{
    @Override
    public String getJob() {
        return "I'm an Employee my job is report the document this weekend!";
    }

    @Override
    public String getPhone() {
        return null;
    }
}
